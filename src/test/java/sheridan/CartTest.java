/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package sheridan;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author mymac
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of setPaymentService method, of class Cart.
     */
    @Test
    public void testSetPaymentService() {
        System.out.println("setPaymentService");
        PaymentService service = DEBIT;
        Cart instance = new Cart();
        instance.setPaymentService(service);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addProduct method, of class Cart.
     */
    @Test
    public void testAddProduct() {
        System.out.println("addProduct");
        Product product = new Product("Soap", 10.67);
        Cart instance = new Cart();
        instance.addProduct(product);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of payCart method, of class Cart.
     */
    @Test
    public void testPayCart() {
        System.out.println("payCart");
        Cart instance = new Cart();
        instance.payCart();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getCartSize method, of class Cart.
     */
    @Test
    public void testGetCartSize() {
        System.out.println("getCartSize");
        Cart instance = new Cart();
        int expResult = 1;
        int result = instance.getCartSize();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
