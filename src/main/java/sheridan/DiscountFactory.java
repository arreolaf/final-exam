/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author mymac
 */
public class DiscountFactory {
    
    private static DiscountFactory instance;
    
    private DiscountFactory(){}
    
    public static DiscountFactory getInstance(){
        if(instance == null){
        instance = new DiscountFactory();
        }
        return instance;
    }
    public Discount getDiscount(DiscountType type){
       switch(type){
           case AMOUNT: return new DiscountByAmount();
           
           case PERCENTAGE: return new DiscountByPercentage();
       }
      return null;

   }

}
