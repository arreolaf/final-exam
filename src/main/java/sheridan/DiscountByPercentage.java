/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan;

/**
 *
 * @author mymac
 */
public class DiscountByPercentage extends Discount{
    
    public double calculateDiscount(double amount){
        double discountPercentage = 20;
        
        double discount = (amount * (discountPercentage/100))-amount;
        
        return discount;
    }
    
    
}
